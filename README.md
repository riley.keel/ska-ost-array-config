SKA SciOps Array Configuration and templates
============================================

A Python package to manipulate SKA LOW and MID array layouts. It accompanies the SKAO memo (**insert document details here**).
The generated layouts are compatible with
 CASA [simutil module](https://casadocs.readthedocs.io/en/v6.5.5/examples/community/simulation_script_demo.html) 
and 
[Radio Astronomy Simulation, Calibration, and Imaging Library (RASCIL)](https://developer.skao.int/projects/rascil/en/latest/index.html)
 if you wish to run your own simulations. You can find a few examples in `docs/examples.ipynb`.

Please contact SKAO Science Operations (sciops@skao.int) if you have comments/questions.

Installation
------------

We use [poetry](https://python-poetry.org/docs/basic-usage/) to manage dependencies in this project. 
You can install `ska_ost_array_config` with

```bash
git clone https://gitlab.com/ska-telescope/ost/ska-ost-array-config.git
cd ska-ost-array-config
poetry install
```

Alternately, you can also install using traditional methods with
```bash
pip install ska-ost-array-config --extra-index-url https://artefact.skao.int/repository/pypi-internal/simple
```

This package has been tested against `python-3.9`, `python-3.10`, and `python-3.11`. 

Developer notes
---------------

If you wish to add more features to this package, feel free to issue a merge request.
We use `black` and `isort` to format our code. Make sure to check the formatting with 

```bash
make python-lint
```


