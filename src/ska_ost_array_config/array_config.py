import fnmatch
import warnings
from pathlib import Path

import matplotlib.pyplot as plt
import numpy
import pandas
from astropy import units
from astropy.coordinates import EarthLocation, SkyCoord
from astropy.time import Time
from astropy.utils.exceptions import AstropyDeprecationWarning
from ska_sdp_datamodels.configuration.config_coordinate_support import (
    ecef_to_enu,
    lla_to_ecef,
)
from ska_sdp_datamodels.configuration.config_model import Configuration

from ska_ost_array_config import UVW, named_subarrays
from ska_ost_array_config.simulation_utils import (
    ExternalTelescope,
    simulate_observation,
)

# MID array reference coordinate
MID_ARRAY_REF = EarthLocation.from_geodetic(
    21.44380306, -30.71292524, 1053.0, ellipsoid="WGS84"
)

# LOW array reference coordinate
LOW_ARRAY_REF = EarthLocation.from_geodetic(
    116.7644482, -26.82472208, 377.8, ellipsoid="WGS84"
)

VALID_SUBARRAYS = ["AA4", "AA*", "AA2", "AA1", "AA0.5", "custom"]


def filter_array_by_distance(full_array, distance, invert=False):
    """
    Generates a list of antennas (from the full_array) that lie within
    the specified distance from the array_centre.


    Parameters
    ----------
    full_array: instance of ska_ost_array_config.array_config.Low or ska_ost_array_config.array_config.Mid
        A valid definition of a subarray
    distance: float or astropy.units.quantity.Quantity equivalent to astropy.units.m
        Distance in metres (for float) from the array_centre within which the filtered
        stations must lie.
    invert: bool
        If invert=True, stations outside the specified distance is returned.
        Default: False

    Return
    ------
    Comma-separated list of filtered station names.
    """
    if isinstance(distance, units.quantity.Quantity):
        if not distance.unit.is_equivalent(units.m):
            raise ValueError(
                f"Input unit of distance is not equivalent to m: {distance}"
            )
        else:
            distance = distance.to(units.m).value

    # Get the list of station/dish names in the full_array
    full_ant_names = full_array.array_config.names.data

    # Dish/station location are stored as offsets with respect to
    # the array centre in XYZ frame. Compute the distance in XY-plane
    # between each station and the array centre
    x = full_array.array_config.xyz[:, 0].data
    y = full_array.array_config.xyz[:, 1].data
    distance_from_centre = numpy.sqrt(x**2 + y**2)
    if invert:
        return ",".join(full_ant_names[distance_from_centre > distance].tolist())
    else:
        return ",".join(full_ant_names[distance_from_centre < distance].tolist())


class SubArray:
    """Parent SubArray class that will be imported by LOWSubArray and MIDSubArray classes"""

    def __init__(
        self,
        array_ref,
        station_names,
        station_coords,
        station_diameter,
        vp_type,
        mount,
        observatory,
        external_telescope,
    ):
        """
        Constructor for SubArray

        Parameters
        ----------
        array_ref : astropy.coordinates.EarthLocation
            Coordinates of the array reference point
        station_names : list of strings
            Names of the stations
        station_coords :
            Array of station coordinates in ENU
        station_diameter :
            Diameter of the dishes or stations in meters
        vp_type : string or list of strings
            Voltage pattern of the dish/station
        mount : string or list of strings
            Type of telescope mount
        observatory : string
            Name of the observatory
        external_telescope : list of ska_ost_array_config.simulation_utils.ExternalTelescope
            List of external non-SKAO telescopes to include in the subarray.
        """
        self.observatory = observatory

        # If specified, add external telescopes to station_names and station_coords
        if external_telescope is not None:
            if not isinstance(external_telescope, list):
                err_msg = (
                    "External telescope specification is invalid. When specifying "
                    + "multiple external facilities, make sure they are passed on as "
                    + "a list."
                )
                raise TypeError(err_msg)
            for telescope in external_telescope:
                if not isinstance(telescope, ExternalTelescope):
                    err_msg = (
                        "At least one of the specified external facility is not of "
                        + "type ska_ost_array_config.simulation_utils.ExternalTelescope."
                    )
                    raise TypeError(err_msg)
                # Append the label and coordinates of the external telescope to
                # station_names and station_coords
                station_names.append(telescope.label)
                station_coords = numpy.concatenate(
                    (station_coords, telescope.to_enu(array_ref))
                )

                # all arrays and strings passed to the Configuration.constructor()
                # below must have the same length. Insert dummy values for vp_type,
                # station_diameter, and mount for now.
                # FIXME: In the future, these three values must be obtained via the
                # ExternalTelescope object.
                station_diameter.append(38.0)
                if self.observatory == "SKA_MID":
                    vp_type.append("dummy")

        self.array_config = Configuration.constructor(
            location=array_ref,
            names=station_names,
            xyz=station_coords,
            diameter=station_diameter,
            vp_type=vp_type,
            mount=mount,
        )

    def generate_casa_antenna_list(self, file_name="antenna_list.txt"):
        """Export antenna coordinates that can be used with
        NRAO CASA simutil module.

        Parameters
        ----------
        file_name : string
            File name to store the antenna coordinates
        """
        n_ants = self.array_config.names.data.shape[0]
        with open(file_name, "w") as f:
            header = f"# observatory={self.observatory}\n"
            header += f"# COFA={self.array_config.location.lon.deg:0.6f},"
            header += f"{self.array_config.location.lat.deg:0.6f}\n"
            header += "# coordsys=LOC\n"
            header += "# x\ty\tz\tDiam\tname\n"
            f.write(header)
            for i in range(n_ants):
                row = f"{self.array_config.xyz.data[i][0]:0.6f}\t"
                row += f"{self.array_config.xyz.data[i][1]:0.6f}\t"
                row += f"{self.array_config.xyz.data[i][2]:0.6f}\t"
                row += f"{self.array_config.diameter.data[i]:0.1f}\t"
                row += f"{self.array_config.names.data[i]}\n"
                f.write(row)

    def plot_array_layout(self, axes=None, scale="kilo", **kwargs):
        """Utility function to plot the array layout

        Parameters
        ----------
        axes : matplotlib.axes._subplots.AxesSubplot
            Matplotlib axes to plot on.
            If None, one will be generated internally.
        scale: string
            Give scaling factor for plotting
            Default: 'kilo' = 1/1000

        Returns
        -------
        Instances of matplotlib.figure.Figure and matplotlib.axes.Axes
        are returned if axes is None.

        Other parameters
        ----------------
        **kwargs : matplotlib.pyplot.scatter properties
        """
        # Create a matplotlib axes if none is provided
        return_vals = False
        if axes is None:
            fig, axes = plt.subplots(1, 1)
            return_vals = True

        if scale.lower() in ["kilo"]:
            uscale = "k"
            fscale = 1000
        else:
            uscale = ""
            fscale = 1

        axes.scatter(
            self.array_config.xyz[:, 0].data / fscale,
            self.array_config.xyz[:, 1].data / fscale,
            s=kwargs.pop("s", 0.2),
            c=kwargs.pop("c", "k"),
            marker=kwargs.pop("marker", "o"),
            alpha=kwargs.pop("alpha", None),
            edgecolors=kwargs.pop("edgecolors", "face"),
            **kwargs,
        )
        axes.set_xlabel(f"X ({uscale}m)")
        axes.set_ylabel(f"Y ({uscale}m)")

        if return_vals:
            return fig, axes

    def plot_snapshot_zenith_uvcov(
        self,
        axes=None,
        ref_freq=50e6,
        chan_width=5.4e3,
        n_chan=55296,
        freq_undersample=100,
        method="lambda",
        scale="kilo",
        plot_conj=True,
        **kwargs,
    ):
        """Plot the snapshot uv coverage for a source at zenith

        Parameters
        ----------
        axes : matplotlib.axes._subplots.AxesSubplot or None
            Matplotlib axes to plot on.
            If None, one will be generated internally.
        ref_freq : float
            Frequency in Hz of the first channel (Default: 50 MHz)
        chan_width : float
            Bandwidth in Hz (Default: 1 MHz)
        n_chan : int
            Number of channels (Default: 55296)
        freq_undersample : int
            Undersampling factor along the frequency dimension.
            Increase this value if your simulation takes too long or
            you are memory limited. Default value of 100 implies only
            one channel is simulated for every 100 channels.
        method : string
            Determines which uv values are plotted. Can be 'metre' or 'lambda'
            Default: 'lambda'
        scale : string
            Give scaling factor for plotting
            Default: 'kilo' = 1/1000
        plot_conj : bool
            Flag to indicate whether the complex conjugate of (u,v) should be plotted too.

        Returns
        -------
        If axes is None, returns instances of the matplotlib.figure.Figure,
            matplotlib.axes.Axes, and UVW.UVW objects.
        Else, return
        Instances of
        are returned if axes is None.

        Other parameters
        ----------------
        **kwargs : matplotlib.pyplot.scatter properties
        """
        # Create a matplotlib axes if none is provided
        return_vals = False
        if axes is None:
            fig, axes = plt.subplots(1, 1)
            return_vals = True

        # Simulate visibilities and retreive the UVW values
        ref_time = Time.now()
        zenith = SkyCoord(
            alt=90 * units.deg,
            az=0 * units.deg,
            frame="altaz",
            obstime=ref_time,
            location=self.array_config.location,
        ).icrs
        vis = simulate_observation(
            array_config=self.array_config,
            phase_centre=zenith,
            start_time=ref_time,
            ref_freq=ref_freq,
            chan_width=chan_width,
            n_chan=n_chan,
            freq_undersample=freq_undersample,
        )
        uvw = UVW.UVW(vis, ignore_autocorr=True)

        UVW.plot_uv_coverage(
            uvw, axes, method=method, scale=scale, plot_conj=plot_conj, **kwargs
        )

        # Return the matplotlib figure if the axes was generated internally
        if return_vals:
            return fig, axes, uvw
        else:
            return uvw


class LowSubArray(SubArray):
    """LowSubArray class"""

    def __init__(
        self, subarray_type="AA*", custom_stations=None, external_telescopes=None
    ):
        """
        Constructor for LowSubArray array config class

        Parameters
        ----------
        subarray_type : string
            Valid subarray types are "AA4", "AA*", "AA2", "AA1", "AA0.5" and "custom".
            If subarray_type="custom", the list of valid station names must
            be specified via custom_stations. Default: AA*
        custom_stations : string
            Valid station names (comma-separated) in a custom subarray.
            This parameter is ignored for AA4 and AA* subarray types.
        external_telescopes : list of ska_ost_array_config.simulation_utils.ExternalTelescope
            List of ExternalTelescope objects defining the external facilities to
            include in simulations. Note that this is not a standard observing mode with
            SKAO telescopes. This functionality is provided in the code purely for
            running simulations.
        """
        assert subarray_type in VALID_SUBARRAYS, "Invalid subarray type specified!"
        self.subarray_type = subarray_type
        if subarray_type == "custom":
            assert (
                custom_stations is not None
            ), "custom_stations must be specified for custom subarrays!"
            self.custom_stations = custom_stations.split(",")
        else:
            self.custom_stations = None
        self.observatory = "SKA_LOW"
        self.external_telescopes = external_telescopes

        # Read-in the coordinate file
        low_coord_file = Path(__file__).resolve().parent / "static/low_array_coords.dat"
        self._full_array_data = pandas.read_table(
            low_coord_file,
            sep=" ",
            comment="#",
            names=["Station ID", "Label", "Longitude", "Latitude"],
        )

        # Parse the subarray type and custom station names
        _station_list = self.__get_station_names()

        # Find the station names and their coordinates
        (
            station_names,
            station_coords,
            station_dia,
        ) = self.__get_station_coordinates(LOW_ARRAY_REF, _station_list)

        # Call the parent to build the configuration
        super().__init__(
            LOW_ARRAY_REF,
            station_names,
            station_coords,
            station_dia,
            "LOW",
            "XY",
            self.observatory,
            self.external_telescopes,
        )

    def __get_station_names(self):
        """Return the list of stations that satisfy the subarray_type
        and custom_stations list"""
        if self.subarray_type == "AA4":
            station_list = self._full_array_data["Label"].to_list()
        elif self.subarray_type in ["AA*", "AA2", "AA1", "AA0.5"]:
            if self.subarray_type == "AA*":
                stations = named_subarrays.LOW_AAstar
            elif self.subarray_type == "AA2":
                stations = named_subarrays.LOW_AA2
            elif self.subarray_type == "AA1":
                stations = named_subarrays.LOW_AA1
            elif self.subarray_type == "AA0.5":
                stations = named_subarrays.LOW_AA05
            else:
                raise ValueError(f"{self.subarray_type} is an invalid subarray type")
            # We need to massage the AAstar case a bit
            # All remote stations have a prefix (E, N, or S). Core stations
            # are just numbers. Add prefix 'C' to maintain uniformity throughout.
            station_list = []
            for station in stations.split(","):
                this_station = station.lstrip().rstrip()
                if this_station[0].isdigit():
                    station_list.append(f"C{this_station}")
                else:
                    station_list.append(this_station)
        elif self.subarray_type == "custom":
            all_stations = self._full_array_data["Label"].to_list()
            station_list = []
            for pattern in self.custom_stations:
                this_selection = fnmatch.filter(all_stations, pattern)
                if len(this_selection) < 1:
                    err_msg = f"{pattern} is not a valid pattern to select stations"
                    raise ValueError(err_msg)
                station_list += this_selection
        else:
            raise ValueError("Invalid subarray type specified in LOW")

        return numpy.unique(station_list).tolist()

    def __get_station_coordinates(self, array_centre, station_list):
        """Workout the LOW station coordinates for all stations in
        station_list. Returns station names, ENU coordinates,
        station diameter."""
        ant_labels = []
        ant_lon = []
        ant_lat = []
        for station in station_list:
            if station[0] == "C":
                mask = self._full_array_data["Label"].str.fullmatch(station)
            else:
                mask = self._full_array_data["Label"].str.startswith(station)
            ant_labels += self._full_array_data["Label"][mask].tolist()
            ant_lon += self._full_array_data["Longitude"][mask].tolist()
            ant_lat += self._full_array_data["Latitude"][mask].tolist()

        ant_lon = numpy.asarray(ant_lon)
        ant_lat = numpy.asarray(ant_lat)

        # Convert the station coordinates in (lon, lat) to ECEF
        # FIXME: Using height=0m for all stations here. Update when heights are known.
        x, y, z = lla_to_ecef(ant_lat * units.deg, ant_lon * units.deg, 0.0)
        ant_xyz = numpy.stack((x, y, z), axis=1)

        # Convert the ECEF coordinates to ENU coordinates
        warnings.filterwarnings("ignore", category=AstropyDeprecationWarning)
        ant_xyz = ecef_to_enu(array_centre, ant_xyz)
        warnings.resetwarnings()

        ant_dia = 38.0 * numpy.ones_like(ant_lon)

        return ant_labels, ant_xyz, ant_dia.tolist()


class MidSubArray(SubArray):
    """MidSubArray class"""

    def __init__(
        self, subarray_type="AA*", custom_stations=None, external_telescopes=None
    ):
        """
        Constructor for MID array config class

        Parameters
        ----------
        subarray_type : string
            Valid subarray types are "AA4", "AA*", "AA2", "AA1", "AA0.5", and "custom".
            If subarray_type="custom", the list of valid station names must
            be specified via custom_stations. Default: AA*
        custom_stations : string
            Valid station names (comma-separated) in a custom subarray.
            This parameter is ignored for AA4 and AA* subarray types.
        external_telescopes : list of ska_ost_array_config.simulation_utils.ExternalTelescope
            List of ExternalTelescope objects defining the external facilities to
            include in simulations. Note that this is not a standard observing mode with
            SKAO telescopes. This functionality is provided in the code purely for
            running simulations.
        """
        assert subarray_type in VALID_SUBARRAYS, "Invalid subarray type specified!"
        self.subarray_type = subarray_type
        if subarray_type == "custom":
            assert (
                custom_stations is not None
            ), "custom_stations must be specified for custom subarrays!"
            self.custom_stations = custom_stations.split(",")
        else:
            self.custom_stations = None
        self.observatory = "SKA_MID"
        self.external_telescopes = external_telescopes

        mid_coord_file = Path(__file__).resolve().parent / "static/mid_array_coords.dat"
        self._full_array_data = pandas.read_table(
            mid_coord_file,
            sep=" ",
            comment="#",
            names=["Label", "Longitude", "Latitude"],
        )

        # Parse the subarray type and custom station names
        _station_list = self.__get_station_names()

        # Find the station names and their coordinates
        (
            station_names,
            station_coords,
            station_dia,
        ) = self.__get_station_coordinates(MID_ARRAY_REF, _station_list)

        vp_type = []
        for name in station_names:
            if name[0] == "M":
                vp_type.append("MEERKAT")
            else:
                vp_type.append("MID")

        # Call the parent to build the configuration
        super().__init__(
            MID_ARRAY_REF,
            station_names,
            station_coords,
            station_dia,
            vp_type,
            "azel",
            self.observatory,
            self.external_telescopes,
        )

    def __get_station_names(self):
        """Return the list of stations that satisfy the subarray_type
        and custom_stations list"""
        if self.subarray_type == "AA4":
            station_list = self._full_array_data["Label"].to_list()
        elif self.subarray_type == "AA*":
            station_list = named_subarrays.MID_AAstar.split(",")
        elif self.subarray_type == "AA2":
            station_list = named_subarrays.MID_AA2.split(",")
        elif self.subarray_type == "AA1":
            station_list = named_subarrays.MID_AA1.split(",")
        elif self.subarray_type == "AA0.5":
            station_list = named_subarrays.MID_AA05.split(",")
        elif self.subarray_type == "custom":
            all_stations = self._full_array_data["Label"].to_list()
            station_list = []
            for pattern in self.custom_stations:
                this_selection = fnmatch.filter(all_stations, pattern)
                if len(this_selection) < 1:
                    err_msg = f"{pattern} is not a valid pattern to select stations"
                    raise ValueError(err_msg)
                station_list += this_selection
        else:
            raise ValueError("Invalid subarray type specified in LOW")

        return numpy.unique(station_list).tolist()

    def __get_station_coordinates(self, array_centre, station_list):
        """Workout the LOW station coordinates for all stations in
        station_list. Returns station names, ENU coordinates,
        station diameter."""
        ant_labels = []
        ant_lon = []
        ant_lat = []
        for station in station_list:
            mask = self._full_array_data["Label"].str.startswith(station)
            if not mask.any():
                raise ValueError(f"{station} does not match")
            ant_labels += self._full_array_data["Label"][mask].tolist()
            ant_lon += self._full_array_data["Longitude"][mask].tolist()
            ant_lat += self._full_array_data["Latitude"][mask].tolist()

        ant_lon = numpy.asarray(ant_lon)
        ant_lat = numpy.asarray(ant_lat)

        # Convert the station coordinates in (lon, lat) to ECEF
        # FIXME: Using height=0m for all stations here. Update when heights are known.
        x, y, z = lla_to_ecef(ant_lat * units.deg, ant_lon * units.deg, 0.0)
        ant_xyz = numpy.stack((x, y, z), axis=1)

        # Convert the ECEF coordinates to ENU coordinates
        warnings.filterwarnings("ignore", category=AstropyDeprecationWarning)
        ant_xyz = ecef_to_enu(array_centre, ant_xyz)
        warnings.resetwarnings()

        # Form dish diameter list
        ant_dia = []
        for name in ant_labels:
            if "M" in name:
                ant_dia.append(13.5)
            else:
                ant_dia.append(15.0)

        return ant_labels, ant_xyz, ant_dia
