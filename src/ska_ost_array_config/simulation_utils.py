import warnings

import astropy.coordinates
import numpy
from astroplan import Observer, TargetAlwaysUpWarning
from astropy import units
from astropy.time import TimeDelta
from astropy.utils.exceptions import AstropyDeprecationWarning
from ska_sdp_datamodels.configuration.config_coordinate_support import (
    ecef_to_enu,
    lla_to_ecef,
)
from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.visibility import create_visibility


class ExternalTelescope:
    """ExternalTelescope class that provides an interface to non-SKAO telescopes"""

    def __init__(self, label, location):
        """
        ExternalTelescope class

        Parameters
        ----------
        label : string
            Name of the telescope
        location : astropy.coordinates.EarthLocation
            Coordinates of the telescope as an EarthLocation object.
        """
        self.label = label
        self.location = location

    def to_enu(self, array_centre):
        """
        Convert the coordinates of the external telescope to ENU coordinates
        with respect to the specified array_centre.

        Parameters
        ----------
        array_centre : astropy.coordinates.EarthLocation

        Returns
        -------
        XYZ coordinates of the external facility in ENU coordinate system with respect
        to the specified array_centre.
        """
        # Convert the coordinates of the external facility in (lon, lat) to ECEF
        x, y, z = lla_to_ecef(
            self.location.lat, self.location.lon, self.location.height.value
        )
        ant_xyz = numpy.array(
            [
                [
                    x,
                ],
                [
                    y,
                ],
                [
                    z,
                ],
            ]
        ).transpose()

        # Convert the ECEF coordinates to ENU coordinates
        warnings.filterwarnings("ignore", category=AstropyDeprecationWarning)
        ant_xyz = ecef_to_enu(array_centre, ant_xyz)
        warnings.resetwarnings()

        return ant_xyz


def simulate_observation(
    array_config,
    phase_centre,
    start_time,
    duration=1.0,
    integration_time=1,
    ref_freq=50e6,
    chan_width=5.4e3,
    n_chan=1000,
    horizon=20,
    freq_undersample=100,
    time_undersample=10,
):
    """Simulate an observation with the SKA telescopes

    Parameters
    ----------
    array_config : ska_sdp_datamodels.configuration.config_model.Configuration
        Array layout
    phase_centre : astropy.coordinates.SkyCoord
        Pointing direction
    start_time : astropy.time.core.Time
        Start time of the observation
    duration : float
        Duration of the observation in seconds. Default: 1 s
    integration_time : float
        Length of each time interval in seconds. Default: 1 s
    ref_freq : float
        Frequency in Hz of the first channel (Default: 50 MHz)
    chan_width : float
        Bandwidth in Hz (Default: 1 MHz)
    n_chan : int
        Number of channels (Default: 1000)
    horizon : float
        Elevation of the horizon in degrees. Default: 20 degrees.
    freq_undersample : int
        Undersampling factor along the frequency dimension.
        Increase this value if your simulation takes too long or
        you are memory limited. Default value of 100 implies only one channel
        is simulated for every 100 channels.
    time_undersample : int
        Undersampling factor along the time dimension.
        Increase this value if your simulation takes too long or
        you are memory limited. Default value of 10 implies only one timeslot
        is simulated for every 10 integration intervals.

    Returns
    -------
    Simulated visibilities (as an instance to
    ska_sdp_datamodels.visibility.vis_model.Visibility)
    """

    assert (
        array_config.names.shape[0] > 1
    ), "Cannot perform interferometric simulations with one station/dish!"

    observatory = Observer(array_config.location)

    # Work out the start and end times of the observation in hour angles
    start_ha = (
        observatory.target_hour_angle(start_time, phase_centre).wrap_at("180d").hour
    )
    end_ha = (
        observatory.target_hour_angle(
            start_time + TimeDelta(duration, format="sec"), phase_centre
        )
        .wrap_at("180d")
        .hour
    )
    time_bins_ha = (numpy.pi / 43200.0) * numpy.arange(
        start_ha * 3600.0,
        end_ha * 3600.0,
        integration_time,
    )[::time_undersample]

    # Frequency setup
    channel_frequencies = numpy.linspace(
        ref_freq,
        ref_freq + (n_chan - 1) * chan_width,
        n_chan,
    )[::freq_undersample]
    channel_width = chan_width * numpy.ones_like(channel_frequencies)

    # Finally, simulate visibilities
    visibility = create_visibility(
        config=array_config,
        times=time_bins_ha,
        times_are_ha=True,
        frequency=channel_frequencies,
        channel_bandwidth=channel_width,
        phasecentre=phase_centre,
        integration_time=integration_time,
        utc_time=start_time,
        polarisation_frame=PolarisationFrame("stokesI"),
        elevation_limit=numpy.deg2rad(horizon),
    )

    return visibility


def find_rise_set_times(location, phase_centre, date, elevation_limit=20.0):
    """
    Find the nearest rise, transit and set times for the specified phase centre

    Parameters
    ----------
    location : astropy.coordinates.EarthLocation
        Location of the observatory
    phase_centre: astropy.coordinates.SkyCoord
        Coordinates of the target
    date: astropy.time.Time
        Reference date to estimate the outputs
    elevation_limit: float
        Minimum elevation in degrees
        Default: 20 degrees

    Returns
    -------
    astropy.time.Time objects corresponding to target rise, transit and set times.
    Rise and set times are set to None if the target is circumpolar. All three elements are
    set to None if the target is never visible from the specified location.
    """
    # Transform location to astroplan.Observer()
    location = Observer(location)
    # Find the next transit time
    transit_time = location.target_meridian_transit_time(
        date, phase_centre, which="next"
    )
    if location.altaz(transit_time, phase_centre).alt.deg < elevation_limit:
        # Target is never above the horizon at this location
        return None, None, None

    # Find the rise and set times associated with this transit time
    warnings.filterwarnings("error")
    try:
        rise_time = location.target_rise_time(
            transit_time,
            phase_centre,
            which="previous",
            horizon=elevation_limit * units.deg,
        )
        set_time = location.target_set_time(
            transit_time,
            phase_centre,
            which="next",
            horizon=elevation_limit * units.deg,
        )
    except TargetAlwaysUpWarning:
        rise_time = None
        set_time = None
    warnings.resetwarnings()

    return rise_time, transit_time, set_time
