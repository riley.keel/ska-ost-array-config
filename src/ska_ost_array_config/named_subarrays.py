# AA0.5 will have 6 stations.
# Taken from https://confluence.skatelescope.org/display/SST/AA0.5+Stations+Locations
LOW_AA05 = "S8-1,S8-6,S9-2,S9-5,S10-3,S10-6"

# AA1 will have 18 stations. The station list was taken from the LOW Roll-Out Plan
# document (SKA-TEL-AIV-4410001 Revision 10)
LOW_AA1 = "S8, S9, S10"

# LOW AA2 will have 64 stations.
# The station list was taken from the LOW Roll-Out Plan
# document (SKA-TEL-AIV-4410001 Revision 10)
LOW_AA2 = (
    "S8, S9, S10, S16,"
    + "66,111,22,69,80,30,78,91,23,36,108,52,193,16,98,56,146,70,200,57,132,62,"
    + "90,73,183,176,17,88,32,31,33,89,59,165,8,158,144,72,4,167"
)

# AA* will have 307 stations. The station list was taken from the
# LOW Roll-Out Plan document (SKA-TEL-AIV-4410001 Revision 10)
# + station 47 ECP pending
LOW_AAstar = (
    "S8,S9,S10,S16,N8,N9,N10,N16,E8,E9,E10,E16,E12,E14,N11,N14,S12,S14,"
    + "66, 111, 22, 69, 80, 30, 78, 91, 23, 36, 108, 52, 193, 16, 98, 56, "
    + "146, 70, 200, 57, 132, 62, 90, 73, 183, 176, 17, 88, 32, 31, 33, 89, "
    + "59, 165, 8, 158, 144, 72, 4, 167, 14, 185, 142, 19, 224, 7, 160, 74, "
    + "42, 171, 79, 96, 27, 75, 85, 212, 15, 63, 205, 24, 29, 25, 112, 187, "
    + "84, 1, 120, 175, 44, 38, 67, 54, 155, 49, 170, 87, 216, 105, 35, 156, "
    + "121, 86, 58, 150, 60, 143, 68, 151, 18, 45, 100, 76, 195, 107, 34, 172, "
    + "20, 64, 191, 115, 2, 125, 48, 71, 61, 130, 55, 202, 223, 140, 37, 41, 46, "
    + "139, 213, 104, 9, 99, 13, 192, 114, 204, 21, 12, 147, 82, 103, 138, 106, "
    + "11, 119, 6, 194, 5, 93, 168, 28, 197, 154, 26, 92, 110, 77, 50, 10, 53, "
    + "95, 166, 40, 128, 162, 113, 222, 129, 214, 102, 65, 109, 97, 123, 141, "
    + "39, 83, 148, 198, 81, 3, 135, 51, 145, 207, 149, 137, 43, 163, 101, 181, "
    + "180, 117, 206, 126, 177, 124, 133, 208, 190, 153, 164, 173, 219, 201, 184, "
    + "217, 203, 186, 199, 161, 179, "
    + "47"
)

# List of MID dishes in AA0.5
# List of antennas from MID Roll-out plan revision 10 (SKA-TEL-AIV-2410001)
MID_AA05 = "SKA001,SKA036,SKA063,SKA100"

# List of MID dishes in AA1
# List of antennas from MID Roll-out plan revision 10 (SKA-TEL-AIV-2410001)
MID_AA1 = MID_AA05 + "," + "SKA046,SKA048,SKA077,SKA081"

# List of MeerKAT antennas
MID_MKAT = (
    "M000,M001,M002,M003,M004,M005,M006,M007,M008,M009,M010,M011,M012,M013,"
    + "M014,M015,M016,M017,M018,M019,M020,M021,M022,M023,M024,M025,M026,M027,"
    + "M028,M029,M030,M031,M032,M033,M034,M035,M036,M037,M038,M039,M040,M041,"
    + "M042,M043,M044,M045,M046,M047,M048,M049,M050,M051,M052,M053,M054,M055,"
    + "M056,M057,M058,M059,M060,M061,M062,M063"
)

# MeerKAT+ antenna list
# https://confluence.skatelescope.org/pages/viewpage.action?pageId=74727334
# Note that the list below comes from a draft report by Sphe, Oleg, and
# HRK from March 2023. We need an SKAO source of truth for this information.
MID_MKAT_PLUS = (
    "SKA017,SKA018,SKA105,SKA107,SKA023,SKA110,SKA115,SKA117,SKA118,SKA119,"
    + "SKA121,SKA020,SKA060,SKA024,SKA026,SKA034"
)

# MID AA2 will have 64 antennas
# List of antennas from MID Roll-out plan revision 10 (SKA-TEL-AIV-2410001)
MID_AA2 = (
    # Batch 1
    "SKA001,SKA036,SKA063,SKA100,"
    # Batch 2
    + "SKA046,SKA048,SKA077,SKA081,"
    # Batch 3
    + "SKA015,SKA025,SKA009,SKA008,"
    # Batch 4
    + "SKA013,SKA014,SKA016,SKA019,SKA027,SKA028,SKA030,SKA032,"
    + "SKA033,SKA035,SKA038,SKA039,SKA040,SKA041,SKA042,SKA043,SKA044,SKA045,"
    + "SKA049,SKA050,SKA051,SKA052,SKA054,SKA055,SKA061,SKA067,SKA068,SKA070,"
    + "SKA075,SKA079,SKA082,SKA083,SKA089,SKA091,SKA092,SKA093,SKA095,SKA096,"
    + "SKA097,SKA098,SKA099,SKA101,SKA102,SKA103,SKA104,SKA106,SKA108,SKA109,"
    + "SKA113,SKA123,SKA125,SKA126"
)

# MID AA* will have 144 antennas (64 AA2 antennas + 64 MeerKAT + 16 MeerKAT+)
# List of antennas from MID Roll-out plan revision 10 (SKA-TEL-AIV-2410001)
MID_AAstar = MID_AA2 + "," + MID_MKAT_PLUS + "," + MID_MKAT
