import matplotlib.pyplot as plt
import numpy


def plot_baseline_distribution(
    uvw, axes=None, method="lambda", scale="kilo", plot_type="uv", **kwargs
):
    """Plot a cumulative distribution of baseline lengths

    Parameters
    ----------
    uvw : UVW.UVW
        Object to class UVW.UVW
    axes : matplotlib.axes._subplots.AxesSubplot
        Matplotlib axes to plot on. If None, one will be generated internally.
    method : string
        Determines which uv values are plotted. Can be 'metre' or 'lambda'
        Default: 'lambda'
    scale : string
        Give scaling factor for plotting
        Default: 'kilo' = 1/1000
    plot_type : string
        Determines which value to plot. Can be 'u', 'v', or 'uv'
        Default: 'uvdist'

    Returns
    -------
    Instances of matplotlib.figure.Figure and matplotlib.axes.Axes
    are returned if axes is None.

    Other parameters
    ----------------
    **kwargs : matplotlib.pyplot.scatter properties
    """
    assert method in [
        "meter",
        "metre",
        "lambda",
    ], "Invalid method specified. Valid methods are metre or lambda."

    assert plot_type in [
        "u",
        "v",
        "uv",
    ], "Invalid plot_type specified. Valid types are u, v, or uv."

    if scale.lower() in ["kilo"]:
        uscale = "k"
        fscale = 1000
    else:
        uscale = ""
        fscale = 1

    if method in ["metre", "meter"]:
        unit = "m"
    else:
        unit = r"$\lambda$"

    # Create a matplotlib axes if none is provided
    return_vals = False
    if axes is None:
        fig, axes = plt.subplots(1, 1)
        return_vals = True

    if plot_type == "uv":
        if method in ["metre", "meter"]:
            data_points = uvw.uvdist_m
        else:
            data_points = uvw.uvdist_lambda
    if plot_type == "u":
        if method in ["metre", "meter"]:
            data_points = numpy.absolute(uvw.u_m)
        else:
            data_points = numpy.absolute(uvw.u_wave)
    if plot_type == "v":
        if method in ["metre", "meter"]:
            data_points = numpy.absolute(uvw.v_m)
        else:
            data_points = numpy.absolute(uvw.v_wave)

    axes.hist(
        data_points / fscale,
        bins=kwargs.pop("bins", 50),
        density=kwargs.pop("density", True),
        cumulative=kwargs.pop("cumulative", True),
        histtype=kwargs.pop("histtype", "step"),
        **kwargs,
    )
    axes.set_xlabel(f"Length ({uscale}{unit})")
    axes.set_ylabel("Cumulative fraction of baselines")

    if return_vals:
        return fig, axes


def plot_uv_coverage(
    uvw, axes=None, method="lambda", scale="kilo", plot_conj=True, **kwargs
):
    """Plot uv coverage

    Parameters
    ----------
    uvw : UVW.UVW
        Object to class UVW.UVW
    axes : matplotlib.axes._subplots.AxesSubplot
        Matplotlib axes to plot on. If None, one will be generated internally.
    method : string
        Determines which uv values are plotted. Can be 'metre' or 'lambda'
        Default: 'lambda'
    scale : string
        Give scaling factor for plotting
        Default: 'kilo' = 1/1000
    plot_conj : bool
        Flag to indicate whether the complex conjugate of (u,v) should be plotted too.

    Returns
    -------
    Instances of matplotlib.figure.Figure and matplotlib.axes.Axes
    are returned if axes is None.

    Other parameters
    ----------------
    **kwargs : matplotlib.pyplot.scatter properties
    """

    if scale.lower() in ["kilo"]:
        uscale = "k"
        fscale = 1000
    else:
        uscale = ""
        fscale = 1

    assert method in [
        "metre",
        "meter",
        "lambda",
    ], "Invalid method specified. Valid methods are metre or lambda."

    # Create a matplotlib axes if none is provided
    return_vals = False
    if axes is None:
        fig, axes = plt.subplots(1, 1)
        return_vals = True

    c_conj = kwargs.pop("c_conj", "k")
    marker_conj = kwargs.pop("marker_conj", "^")
    if method in ["meter", "metre"]:
        unit = "m"
        axes.scatter(
            uvw.u_m / fscale,
            uvw.v_m / fscale,
            s=kwargs.pop("s", 0.2),
            c=kwargs.pop("c", "k"),
            marker=kwargs.pop("marker", "o"),
            **kwargs,
        )
        if plot_conj:
            axes.scatter(
                -1 * uvw.u_m / fscale,
                -1 * uvw.v_m / fscale,
                s=kwargs.pop("s", 0.2),
                c=c_conj,
                marker=marker_conj,
                **kwargs,
            )
        axis_limit = numpy.max(
            [
                numpy.max(numpy.abs(uvw.u_m) / fscale),
                numpy.max(numpy.abs(uvw.v_m) / fscale),
            ]
        )
        axis_limit += 0.1 * axis_limit
        axes.set_xlim(-axis_limit, axis_limit)
        axes.set_ylim(-axis_limit, axis_limit)
    else:
        unit = r"$\lambda$"
        axes.scatter(
            uvw.u_wave / fscale,
            uvw.v_wave / fscale,
            s=kwargs.pop("s", 0.2),
            c=kwargs.pop("c", "k"),
            marker=kwargs.pop("marker", "o"),
            **kwargs,
        )
        if plot_conj:
            axes.scatter(
                -1 * uvw.u_wave / fscale,
                -1 * uvw.v_wave / fscale,
                s=kwargs.pop("s", 0.2),
                c=c_conj,
                marker=marker_conj,
                **kwargs,
            )
        axis_limit = numpy.max(
            [
                numpy.max(numpy.abs(uvw.u_wave) / fscale),
                numpy.max(numpy.abs(uvw.v_wave) / fscale),
            ]
        )
        axis_limit += 0.1 * axis_limit
        axes.set_xlim(-axis_limit, axis_limit)
        axes.set_ylim(-axis_limit, axis_limit)
    axes.set_xlabel(f"U ({uscale}{unit})")
    axes.set_ylabel(f"V ({uscale}{unit})")

    # Enforce square plots
    axes.set_box_aspect(1)

    if return_vals:
        return fig, axes


class UVW:
    """UVW class to support manipulation of UVW coordinates"""

    def __init__(
        self,
        visibility,
        ignore_autocorr=False,
    ):
        """Constructor for UVW class

        Parameters
        ----------
        visibility : ska_sdp_datamodels.visibility.vis_model.Visibility
            Visibility data
        ignore_autocorr : bool
            Flag to exclude autocorrelations.
        """
        # Parse visibility and get the uvw values in metres
        self.u_m = visibility.uvw[:, :, 0].to_numpy().flatten()
        self.v_m = visibility.uvw[:, :, 1].to_numpy().flatten()
        if ignore_autocorr:
            autocorr_flags = numpy.logical_and(self.u_m == 0, self.v_m == 0)
            self.u_m = self.u_m[~autocorr_flags]
            self.v_m = self.v_m[~autocorr_flags]

        # Convert uv coordinates to uvwave
        c = 299792458.0  # [m/s]
        self.u_wave = numpy.outer(self.u_m, visibility.frequency.data / c).flatten()
        self.v_wave = numpy.outer(self.v_m, visibility.frequency.data / c).flatten()

        # Compute baseline lengths
        self.uvdist_m = numpy.sqrt(self.u_m**2 + self.v_m**2).flatten()
        self.uvdist_lambda = numpy.sqrt(self.u_wave**2 + self.v_wave**2).flatten()
