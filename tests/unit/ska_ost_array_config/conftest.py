import datetime
import os
from pathlib import Path

import pytest
from astropy import units
from astropy.coordinates import SkyCoord
from astropy.time import Time

from ska_ost_array_config.array_config import MidSubArray
from ska_ost_array_config.simulation_utils import simulate_observation


@pytest.fixture(scope="session")
def test_observation():
    phase_centre = SkyCoord("04:00:00 -80:00:00", unit=(units.hourangle, units.deg))
    ref_time = Time(datetime.datetime(2023, 5, 21, 19))
    obs = simulate_observation(
        array_config=MidSubArray(subarray_type="AA*").array_config,
        phase_centre=phase_centre,
        start_time=ref_time,
        duration=360.0,
        integration_time=1,
        ref_freq=1420e6,
        chan_width=13.4e3,
        n_chan=200,
        horizon=20,
        freq_undersample=100,
        time_undersample=10,
    )
    yield obs


@pytest.fixture(scope="session")
def test_image_name():
    file_name = Path("test_image.png")
    yield file_name
    os.remove(file_name)


@pytest.fixture(scope="session")
def test_casa_ant_name():
    file_name = Path("ant_file.txt")
    yield file_name
    os.remove(file_name)
