import filecmp
import pickle
from pathlib import Path

import numpy
import pytest
from astropy import units
from astropy.coordinates import EarthLocation
from matplotlib.testing.compare import compare_images

from ska_ost_array_config.array_config import (
    LowSubArray,
    MidSubArray,
    filter_array_by_distance,
)
from ska_ost_array_config.simulation_utils import ExternalTelescope

from .test_simulation_utils import TOLERANCE


def test_n_antennas_in_AA():
    """Test if Mid and Low have correct number of antennas in each AA"""
    # Low AA0.5 has 6 stations
    assert len(LowSubArray(subarray_type="AA0.5").array_config.names.data) == 6
    # Low AA1 has 18 stations
    assert len(LowSubArray(subarray_type="AA1").array_config.names.data) == 18
    # Low AA2 has 64 stations
    assert len(LowSubArray(subarray_type="AA2").array_config.names.data) == 64
    # Low AA* has 307 stations
    assert len(LowSubArray(subarray_type="AA*").array_config.names.data) == 307
    # Low AA4 has 512 stations
    assert len(LowSubArray(subarray_type="AA4").array_config.names.data) == 512

    # Mid AA0.5 has 4 stations
    assert len(MidSubArray(subarray_type="AA0.5").array_config.names.data) == 4
    # Mid AA1 has 8 stations
    assert len(MidSubArray(subarray_type="AA1").array_config.names.data) == 8
    # Mid AA2 has 68 stations
    # (but use 64 since we don't know which 4 MeerKAT dishes will be included)
    assert len(MidSubArray(subarray_type="AA2").array_config.names.data) == 68 - 4
    # Mid AA* has 144 stations
    assert len(MidSubArray(subarray_type="AA*").array_config.names.data) == 144
    # Mid AA4 has 197 stations
    assert len(MidSubArray(subarray_type="AA4").array_config.names.data) == 197


def test_external_facility_failure():
    """Test that LowSubArray and MidSubArray classes fail if the specified external
    facility if of the wrong format"""
    extern_1 = ExternalTelescope(
        label="N16-1",
        location=EarthLocation.from_geodetic(
            lon=116.659076, lat=-26.497529, ellipsoid="WGS84"
        ),
    )

    with pytest.raises(TypeError):
        LowSubArray(
            subarray_type="custom", custom_stations="C*", external_telescopes=extern_1
        )

    with pytest.raises(TypeError):
        LowSubArray(
            subarray_type="custom",
            custom_stations="C*",
            external_telescopes=[extern_1.location],
        )


def test_low_external_telescope():
    """Test if specifying an external telescope works as intended"""
    # We will use a SKA LOW station as an external facility so that we can verify the outcome
    extern_1 = ExternalTelescope(
        label="N16-4",
        location=EarthLocation.from_geodetic(
            lon=116.659577, lat=-26.497756, ellipsoid="WGS84"
        ),
    )

    low_test = LowSubArray(
        subarray_type="custom",
        custom_stations="N16-2,N16-3",
        external_telescopes=[extern_1],
    )

    low_custom = LowSubArray(
        subarray_type="custom", custom_stations="N16-2,N16-3,N16-4"
    )

    assert (
        numpy.absolute(
            low_custom.array_config.xyz.data - low_test.array_config.xyz.data
        )
        < TOLERANCE
    ).all()


def test_mid_external_telescope():
    """Test if specifying an external telescope works as intended"""
    # We will use a SKA MID station as an external facility so that we can verify the outcome
    extern_1 = ExternalTelescope(
        label="M002",
        location=EarthLocation.from_geodetic(
            lon=21.443554, lat=-30.713078, ellipsoid="WGS84"
        ),
    )

    mid_test = MidSubArray(
        subarray_type="custom",
        custom_stations="M000,M001",
        external_telescopes=[extern_1],
    )

    mid_custom = MidSubArray(subarray_type="custom", custom_stations="M000,M001,M002")

    assert (
        numpy.absolute(
            mid_custom.array_config.xyz.data - mid_test.array_config.xyz.data
        )
        < TOLERANCE
    ).all()


def test_low_custom_layout():
    """Test the LOW custom layout"""
    low = LowSubArray(subarray_type="custom", custom_stations="E1-*,C66")
    pickle_file = Path(__file__).resolve().parent.parent / "static/low_custom.pickle"
    with open(pickle_file, "rb") as f:
        expected_low = pickle.load(f)
    assert (low.array_config.names == expected_low.array_config.names).all()
    assert (
        low.array_config.xyz.data - expected_low.array_config.xyz.data < TOLERANCE
    ).all()
    assert (low.array_config.diameter == expected_low.array_config.diameter).all()
    assert (low.array_config.mount == expected_low.array_config.mount).all()
    assert (low.array_config.vp_type == expected_low.array_config.vp_type).all()
    assert (low.array_config.location.x == expected_low.array_config.location.x).all()
    assert (low.array_config.location.y == expected_low.array_config.location.y).all()
    assert (low.array_config.location.z == expected_low.array_config.location.z).all()


def test_low_aa05_layout():
    """Test the LOW AA0.5 layout"""
    low = LowSubArray(subarray_type="AA0.5")
    pickle_file = Path(__file__).resolve().parent.parent / "static/low_aa05.pickle"
    with open(pickle_file, "rb") as f:
        expected_low = pickle.load(f)
    assert (low.array_config.names == expected_low.array_config.names).all()
    assert (
        low.array_config.xyz.data - expected_low.array_config.xyz.data < TOLERANCE
    ).all()
    assert (low.array_config.diameter == expected_low.array_config.diameter).all()
    assert (low.array_config.mount == expected_low.array_config.mount).all()
    assert (low.array_config.vp_type == expected_low.array_config.vp_type).all()
    assert (low.array_config.location.x == expected_low.array_config.location.x).all()
    assert (low.array_config.location.y == expected_low.array_config.location.y).all()
    assert (low.array_config.location.z == expected_low.array_config.location.z).all()


def test_low_aa1_layout():
    """Test the LOW AA1 layout"""
    low = LowSubArray(subarray_type="AA1")
    pickle_file = Path(__file__).resolve().parent.parent / "static/low_aa1.pickle"
    with open(pickle_file, "rb") as f:
        expected_low = pickle.load(f)
    assert (low.array_config.names == expected_low.array_config.names).all()
    assert (
        low.array_config.xyz.data - expected_low.array_config.xyz.data < TOLERANCE
    ).all()
    assert (low.array_config.diameter == expected_low.array_config.diameter).all()
    assert (low.array_config.mount == expected_low.array_config.mount).all()
    assert (low.array_config.vp_type == expected_low.array_config.vp_type).all()
    assert (low.array_config.location.x == expected_low.array_config.location.x).all()
    assert (low.array_config.location.y == expected_low.array_config.location.y).all()
    assert (low.array_config.location.z == expected_low.array_config.location.z).all()


def test_low_aa2_layout():
    """Test the LOW AA2 layout"""
    low = LowSubArray(subarray_type="AA2")
    pickle_file = Path(__file__).resolve().parent.parent / "static/low_aa2.pickle"
    with open(pickle_file, "rb") as f:
        expected_low = pickle.load(f)
    assert (low.array_config.names == expected_low.array_config.names).all()
    assert (
        low.array_config.xyz.data - expected_low.array_config.xyz.data < TOLERANCE
    ).all()
    assert (low.array_config.diameter == expected_low.array_config.diameter).all()
    assert (low.array_config.mount == expected_low.array_config.mount).all()
    assert (low.array_config.vp_type == expected_low.array_config.vp_type).all()
    assert (low.array_config.location.x == expected_low.array_config.location.x).all()
    assert (low.array_config.location.y == expected_low.array_config.location.y).all()
    assert (low.array_config.location.z == expected_low.array_config.location.z).all()


def test_low_aastar_layout():
    """Test the LOW AA* layout"""
    low = LowSubArray(subarray_type="AA*")
    pickle_file = Path(__file__).resolve().parent.parent / "static/low_aastar.pickle"
    with open(pickle_file, "rb") as f:
        expected_low = pickle.load(f)
    assert (low.array_config.names == expected_low.array_config.names).all()
    assert (
        low.array_config.xyz.data - expected_low.array_config.xyz.data < TOLERANCE
    ).all()
    assert (low.array_config.diameter == expected_low.array_config.diameter).all()
    assert (low.array_config.mount == expected_low.array_config.mount).all()
    assert (low.array_config.vp_type == expected_low.array_config.vp_type).all()
    assert (low.array_config.location.x == expected_low.array_config.location.x).all()
    assert (low.array_config.location.y == expected_low.array_config.location.y).all()
    assert (low.array_config.location.z == expected_low.array_config.location.z).all()


def test_low_aa4_layout():
    """Test the LOW AA4 layout"""
    low = LowSubArray(subarray_type="AA4")
    pickle_file = Path(__file__).resolve().parent.parent / "static/low_aa4.pickle"
    with open(pickle_file, "rb") as f:
        expected_low = pickle.load(f)
    assert (low.array_config.names == expected_low.array_config.names).all()
    assert (
        low.array_config.xyz.data - expected_low.array_config.xyz.data < TOLERANCE
    ).all()
    assert (low.array_config.diameter == expected_low.array_config.diameter).all()
    assert (low.array_config.mount == expected_low.array_config.mount).all()
    assert (low.array_config.vp_type == expected_low.array_config.vp_type).all()
    assert (low.array_config.location.x == expected_low.array_config.location.x).all()
    assert (low.array_config.location.y == expected_low.array_config.location.y).all()
    assert (low.array_config.location.z == expected_low.array_config.location.z).all()


def test_mid_custom_layout():
    """Test the MID custom layout"""
    mid = MidSubArray(subarray_type="custom", custom_stations="M*,SKA001")
    pickle_file = Path(__file__).resolve().parent.parent / "static/mid_custom.pickle"
    with open(pickle_file, "rb") as f:
        expected_mid = pickle.load(f)
    assert (mid.array_config.names == expected_mid.array_config.names).all()
    assert (
        mid.array_config.xyz.data - expected_mid.array_config.xyz.data < TOLERANCE
    ).all()
    assert (mid.array_config.diameter == expected_mid.array_config.diameter).all()
    assert (mid.array_config.mount == expected_mid.array_config.mount).all()
    assert (mid.array_config.vp_type == expected_mid.array_config.vp_type).all()
    assert (mid.array_config.location.x == expected_mid.array_config.location.x).all()
    assert (mid.array_config.location.y == expected_mid.array_config.location.y).all()
    assert (mid.array_config.location.z == expected_mid.array_config.location.z).all()


def test_mid_aa05_layout():
    """Test the MID AA0.5 layout"""
    mid = MidSubArray(subarray_type="AA0.5")
    pickle_file = Path(__file__).resolve().parent.parent / "static/mid_aa05.pickle"
    with open(pickle_file, "rb") as f:
        expected_mid = pickle.load(f)
    assert (mid.array_config.names == expected_mid.array_config.names).all()
    assert (
        mid.array_config.xyz.data - expected_mid.array_config.xyz.data < TOLERANCE
    ).all()
    assert (mid.array_config.diameter == expected_mid.array_config.diameter).all()
    assert (mid.array_config.mount == expected_mid.array_config.mount).all()
    assert (mid.array_config.vp_type == expected_mid.array_config.vp_type).all()
    assert (mid.array_config.location.x == expected_mid.array_config.location.x).all()
    assert (mid.array_config.location.y == expected_mid.array_config.location.y).all()
    assert (mid.array_config.location.z == expected_mid.array_config.location.z).all()


def test_mid_aa1_layout():
    """Test the MID AA1 layout"""
    mid = MidSubArray(subarray_type="AA1")
    pickle_file = Path(__file__).resolve().parent.parent / "static/mid_aa1.pickle"
    with open(pickle_file, "rb") as f:
        expected_mid = pickle.load(f)
    assert (mid.array_config.names == expected_mid.array_config.names).all()
    assert (
        mid.array_config.xyz.data - expected_mid.array_config.xyz.data < TOLERANCE
    ).all()
    assert (mid.array_config.diameter == expected_mid.array_config.diameter).all()
    assert (mid.array_config.mount == expected_mid.array_config.mount).all()
    assert (mid.array_config.vp_type == expected_mid.array_config.vp_type).all()
    assert (mid.array_config.location.x == expected_mid.array_config.location.x).all()
    assert (mid.array_config.location.y == expected_mid.array_config.location.y).all()
    assert (mid.array_config.location.z == expected_mid.array_config.location.z).all()


def test_mid_aa2_layout():
    """Test the MID AA2 layout"""
    mid = MidSubArray(subarray_type="AA2")
    pickle_file = Path(__file__).resolve().parent.parent / "static/mid_aa2.pickle"
    with open(pickle_file, "rb") as f:
        expected_mid = pickle.load(f)
    assert (mid.array_config.names == expected_mid.array_config.names).all()
    assert (
        mid.array_config.xyz.data - expected_mid.array_config.xyz.data < TOLERANCE
    ).all()
    assert (mid.array_config.diameter == expected_mid.array_config.diameter).all()
    assert (mid.array_config.mount == expected_mid.array_config.mount).all()
    assert (mid.array_config.vp_type == expected_mid.array_config.vp_type).all()
    assert (mid.array_config.location.x == expected_mid.array_config.location.x).all()
    assert (mid.array_config.location.y == expected_mid.array_config.location.y).all()
    assert (mid.array_config.location.z == expected_mid.array_config.location.z).all()


def test_mid_aastar_layout():
    """Test the MID AA* layout"""
    mid = MidSubArray(subarray_type="AA*")
    pickle_file = Path(__file__).resolve().parent.parent / "static/mid_aastar.pickle"
    with open(pickle_file, "rb") as f:
        expected_mid = pickle.load(f)
    assert (mid.array_config.names == expected_mid.array_config.names).all()
    assert (
        mid.array_config.xyz.data - expected_mid.array_config.xyz.data < TOLERANCE
    ).all()
    assert (mid.array_config.diameter == expected_mid.array_config.diameter).all()
    assert (mid.array_config.mount == expected_mid.array_config.mount).all()
    assert (mid.array_config.vp_type == expected_mid.array_config.vp_type).all()
    assert (mid.array_config.location.x == expected_mid.array_config.location.x).all()
    assert (mid.array_config.location.y == expected_mid.array_config.location.y).all()
    assert (mid.array_config.location.z == expected_mid.array_config.location.z).all()


def test_mid_aa4_layout():
    """Test the MID AA4 layout"""
    mid = MidSubArray(subarray_type="AA4")
    pickle_file = Path(__file__).resolve().parent.parent / "static/mid_aa4.pickle"
    with open(pickle_file, "rb") as f:
        expected_mid = pickle.load(f)
    assert (mid.array_config.names == expected_mid.array_config.names).all()
    assert (
        mid.array_config.xyz.data - expected_mid.array_config.xyz.data < TOLERANCE
    ).all()
    assert (mid.array_config.diameter == expected_mid.array_config.diameter).all()
    assert (mid.array_config.mount == expected_mid.array_config.mount).all()
    assert (mid.array_config.vp_type == expected_mid.array_config.vp_type).all()
    assert (mid.array_config.location.x == expected_mid.array_config.location.x).all()
    assert (mid.array_config.location.y == expected_mid.array_config.location.y).all()
    assert (mid.array_config.location.z == expected_mid.array_config.location.z).all()


def test_mid_invalid_custom_string():
    """Test the MID custom string"""
    with pytest.raises(ValueError) as err:
        MidSubArray(subarray_type="custom", custom_stations="C*")


def test_low_invalid_custom_string():
    """Test the LOW custom string"""
    with pytest.raises(ValueError) as err:
        LowSubArray(subarray_type="custom", custom_stations="M*")


def test_low_invalid_subarray_type():
    """Test the low subarray type"""
    with pytest.raises(AssertionError) as err:
        LowSubArray(subarray_type="error_string")


def test_mid_invalid_subarray_type():
    """Test the low subarray type"""
    with pytest.raises(AssertionError) as err:
        LowSubArray(subarray_type="error_string")


def test_array_layout(test_image_name):
    low = LowSubArray(subarray_type="AA*")
    fig, _ = low.plot_array_layout(scale="")
    fig.savefig(test_image_name)
    reference_image = (
        Path(__file__).resolve().parent.parent / "static/low_reference_array_layout.png"
    )
    assert compare_images(test_image_name, reference_image, tol=TOLERANCE) is None


def test_low_casa_file(test_casa_ant_name):
    low_custom = LowSubArray(subarray_type="custom", custom_stations="C*,E1-*")
    low_custom.generate_casa_antenna_list(test_casa_ant_name)
    reference_file = (
        Path(__file__).resolve().parent.parent / "static/low_reference.casa.cfg"
    )
    assert filecmp.cmp(test_casa_ant_name, reference_file, shallow=False)


def test_mid_casa_file(test_casa_ant_name):
    mid_custom = MidSubArray(subarray_type="custom", custom_stations="M*,SKA017")
    mid_custom.generate_casa_antenna_list(test_casa_ant_name)
    reference_file = (
        Path(__file__).resolve().parent.parent / "static/mid_reference.casa.cfg"
    )
    assert filecmp.cmp(test_casa_ant_name, reference_file, shallow=False)


def test_filter_array_by_distance_float():
    mid_aastar = MidSubArray(subarray_type="AA*")
    expected_result = sorted(
        (
            "M000,M001,M002,M003,M004,M005,M006,M018,M029,SKA049,SKA050,SKA051,"
            + "SKA075,SKA079,SKA081,SKA082"
        ).split(",")
    )

    mid_core_r125 = filter_array_by_distance(mid_aastar, distance=125.0)
    assert expected_result == sorted(mid_core_r125.split(","))


def test_filter_array_by_distance_units():
    mid_aastar = MidSubArray(subarray_type="AA*")
    expected_result = sorted(
        (
            "M000,M001,M002,M003,M004,M005,M006,M018,M029,SKA049,SKA050,SKA051,"
            + "SKA075,SKA079,SKA081,SKA082"
        ).split(",")
    )
    mid_core_r125 = filter_array_by_distance(mid_aastar, distance=125.0 * units.m)
    assert expected_result == sorted(mid_core_r125.split(","))
