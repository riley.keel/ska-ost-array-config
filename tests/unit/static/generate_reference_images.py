import pickle

from ska_ost_array_config.array_config import LowSubArray
from ska_ost_array_config.UVW import UVW, plot_baseline_distribution, plot_uv_coverage

###########################################################
#
# Generate reference images for UVW.plot_array_layout()
#
###########################################################
low = LowSubArray(subarray_type="AA*")
fig, _ = low.plot_array_layout(scale="")
fig.savefig("low_reference_array_layout.png")

###########################################################
#
# Generate reference images for UVW.plot_uv_coverage()
#
###########################################################
with open("mid_observation.pickle", "rb") as f:
    observation = pickle.load(f)
uvw = UVW(observation, ignore_autocorr=True)

# Generate uv plot with kilo lambda units
fig, axes = plot_uv_coverage(uvw)
fig.savefig("mid_reference_uv_plot_klambda.png")

# Generate uv plot with lambda units
fig, axes = plot_uv_coverage(uvw, scale="")
fig.savefig("mid_reference_uv_plot_lambda.png")

# Generate uv plot with meter units
fig, axes = plot_uv_coverage(uvw, scale="", method="meter")
fig.savefig("mid_reference_uv_plot_meter.png")

# Generate uv plot with meter units
fig, axes = plot_uv_coverage(uvw, scale="kilo", method="meter")
fig.savefig("mid_reference_uv_plot_kmeter.png")

################################################################
#
# Generate reference images for UVW.plot_baseline_distribution()
#
################################################################
fig, axes = plot_baseline_distribution(
    uvw, method="lambda", scale="kilo", plot_type="uv"
)
plot_baseline_distribution(uvw, method="lambda", scale="kilo", plot_type="u", axes=axes)
plot_baseline_distribution(uvw, method="lambda", scale="kilo", plot_type="v", axes=axes)
fig.savefig("mid_reference_uv_hist_klambda.png")

fig, axes = plot_baseline_distribution(uvw, method="metre", scale="", plot_type="uv")
plot_baseline_distribution(uvw, method="metre", scale="", plot_type="u", axes=axes)
plot_baseline_distribution(uvw, method="metre", scale="", plot_type="v", axes=axes)
fig.savefig("mid_reference_uv_hist_metre.png")
